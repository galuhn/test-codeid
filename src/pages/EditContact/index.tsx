import React, { useEffect, useState } from "react";
import { Dimensions, Text, View, TextInput, TouchableOpacity, StyleSheet } from "react-native";
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from "../../store/store";
import { getContact, updateContact } from "../../store/slices/contact";

const EditContactPage = ({ navigation, route }: any) => {
    const dispatch: AppDispatch = useDispatch();
    const { id } = route.params; // Dapatkan ID kontak dari params navigasi

    // Ambil state dari Redux
    const { loading, contact, error } = useSelector((state: RootState) => state.contact);

    // Inisialisasi state dengan data dari Redux
    const initialContact = contact || { id: 0, firstName: '', lastName: '', age: '' };
    const [firstName, setFirstName] = useState(initialContact.firstName);
    const [lastName, setLastName] = useState(initialContact.lastName);
    const [age, setAge] = useState(initialContact.age);

    // Fetch data kontak berdasarkan ID saat komponen dimount
    useEffect(() => {
        if (id) {
            dispatch(getContact(id));
        }
    }, [dispatch, id]);

    // Update state lokal ketika data kontak berubah
    useEffect(() => {
        // Update state dengan data kontak yang diambil
        setFirstName(initialContact.firstName);
        setLastName(initialContact.lastName);
        setAge(initialContact.age);
    }, [initialContact]);

    // Handle penyimpanan informasi kontak yang diperbarui
    const handleSaveContact = async () => {
        try {
            await dispatch(updateContact({ id, updatedContact: { id, firstName, lastName, age } }));
            navigation.goBack(); // Kembali setelah pembaruan berhasil
        } catch (error) {
            console.error('Error saving contact:', error);
            // Handle error jika diperlukan
        }
    };

    return (
        <View style={{ flex: 1, backgroundColor: "white" }}>
            <View style={{ marginTop: 20 }}>
                <Text style={{ color: "black", paddingHorizontal: 15 }}>First Name</Text>
                <View style={{ alignItems: "center", marginTop: 10, justifyContent: "center" }}>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.input}
                            onChangeText={(text) => setFirstName(text)}
                            value={firstName}
                            placeholder="Enter first name"
                            placeholderTextColor="#000"
                        />
                    </View>
                </View>
            </View>

            <View style={{ marginTop: 15 }}>
                <Text style={{ color: "black", paddingHorizontal: 15 }}>Last Name</Text>
                <View style={{ alignItems: "center", marginTop: 10, justifyContent: "center" }}>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.input}
                            onChangeText={(text) => setLastName(text)}
                            value={lastName}
                            placeholder="Enter last name"
                            placeholderTextColor="#000"
                        />
                    </View>
                </View>
            </View>

            <View style={{ marginTop: 15 }}>
                <Text style={{ color: "black", paddingHorizontal: 15 }}>Age</Text>
                <View style={{ alignItems: "center", marginTop: 10, justifyContent: "center" }}>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.input}
                            onChangeText={(text) => setAge(text)}
                            value={age}
                            placeholder="Enter age"
                            placeholderTextColor="#000"
                            keyboardType="number-pad"
                        />
                    </View>
                </View>
            </View>

            {/* Save and Cancel Buttons */}
            <View style={styles.buttonContainer}>
                <TouchableOpacity onPress={() => navigation.goBack()} style={styles.cancelButton}>
                    <Text style={{ color: "red" }}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={handleSaveContact} style={styles.saveButton}>
                    <Text style={styles.buttonText}>Save</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default EditContactPage;

const styles = StyleSheet.create({
    inputContainer: {
        borderWidth: 1,
        width: Dimensions.get("screen").width * 0.9,
        flexDirection: "row",
        borderRadius: 10,
        borderColor: "gray",
    },
    input: {
        height: 48,
        color: "black",
        flex: 1,
        marginLeft: 10,
    },
    buttonContainer: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: 'white',
        width: Dimensions.get('window').width,
        height: 65,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    cancelButton: {
        backgroundColor: 'transparent',
    },
    saveButton: {
        backgroundColor: 'red',
        width: Dimensions.get('window').width * 0.7,
        height: 40,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontFamily: 'Poppins-Regular',
        fontSize: 12,
    },
});
