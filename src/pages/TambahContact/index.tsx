import React, { useState } from "react";
import { Dimensions, Text, View, TextInput, TouchableOpacity, Image } from "react-native";
import { launchImageLibrary, ImagePickerResponse } from 'react-native-image-picker';
import { useDispatch } from 'react-redux';
import { createContact, fetchData } from "../../store/slices/contact";

const defaultPhotoUrl = "https://cdn-cas.orami.co.id/parenting/images/kucing_gemas-1.width-800.jpg";

const TambahContactPage = ({ navigation }: any) => {
  const dispatch = useDispatch();
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [age, setAge] = useState("");
  const [photo, setPhoto] = useState<string>(defaultPhotoUrl); // State untuk menyimpan URL gambar, default ke URL kucing

  const handleChoosePhoto = () => {
    launchImageLibrary({
      mediaType: 'photo',
      includeBase64: false,
      maxHeight: 200,
      maxWidth: 200,
    }, (response: ImagePickerResponse) => {
      if (!response.didCancel && !response.errorCode) {
        setPhoto(response.uri || defaultPhotoUrl); // Set URL gambar ke dalam state photo, jika tidak ada, gunakan default
      }
    });
  };

  const handleSaveContact = async () => {
    try {
      dispatch(createContact({ firstName, lastName, age, photo }));
      fetchData()
      navigation.goBack(); // Kembali ke halaman sebelumnya setelah berhasil menyimpan
    } catch (error) {
      console.error('Error saving contact:', error);
      // Handle error jika diperlukan
    }
  };

  return (
    <View style={{ flex: 1, backgroundColor: "white" }}>
      <View style={{ marginTop: 15, alignItems: 'center' }}>
        {photo && <Image source={{ uri: photo }} style={{ width: 100, height: 100, borderRadius: 50 }} />}
        <TouchableOpacity onPress={handleChoosePhoto} style={{ marginTop: 10 }}>
          <Text style={{ color: 'blue' }}>Pilih Foto</Text>
        </TouchableOpacity>
      </View>

      <View style={{ marginTop: 20 }}>
        <Text style={{ color: "black", paddingHorizontal: 15 }}>First Name</Text>
        <View style={{ alignItems: "center", marginTop: 10, justifyContent: "center" }}>
          <View style={{
            borderWidth: 1,
            width: Dimensions.get("screen").width * 0.90,
            flexDirection: "row",
            borderRadius: 10,
            borderColor: "gray",
          }}>
            <TextInput
              style={{
                height: 48,
                color: "black",
                flex: 1,
                marginLeft: 10
              }}
              onChangeText={(text) => setFirstName(text)}
              value={firstName}
              placeholder="Masukan first name"
              placeholderTextColor="#000"
            />
          </View>
        </View>
      </View>

      <View style={{ marginTop: 15 }}>
        <Text style={{ color: "black", paddingHorizontal: 15 }}>Last Name</Text>
        <View style={{ alignItems: "center", marginTop: 10, justifyContent: "center" }}>
          <View style={{
            borderWidth: 1,
            width: Dimensions.get("screen").width * 0.90,
            flexDirection: "row",
            borderRadius: 10,
            borderColor: "gray",
          }}>
            <TextInput
              style={{
                height: 48,
                color: "black",
                flex: 1,
                marginLeft: 10
              }}
              onChangeText={(text) => setLastName(text)}
              value={lastName}
              placeholder="Masukan last name"
              placeholderTextColor="#000"
            />
          </View>
        </View>
      </View>

      <View style={{ marginTop: 15 }}>
        <Text style={{ color: "black", paddingHorizontal: 15 }}>Age</Text>
        <View style={{ alignItems: "center", marginTop: 10, justifyContent: "center" }}>
          <View style={{
            borderWidth: 1,
            width: Dimensions.get("screen").width * 0.90,
            flexDirection: "row",
            borderRadius: 10,
            borderColor: "gray",
          }}>
            <TextInput
              style={{
                height: 48,
                color: "black",
                flex: 1,
                marginLeft: 10
              }}
              onChangeText={(text) => setAge(text)}
              value={age}
              placeholder="Masukan age"
              placeholderTextColor="#000"
              keyboardType="number-pad"
            />
          </View>
        </View>
      </View>

      <View style={{
        position: 'absolute', bottom: 0, backgroundColor: 'white', width: Dimensions.get('window').width, height: 65, shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
      }}>
        <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 15, flexDirection: 'row' }}>
          <TouchableOpacity onPress={() => {
            navigation.goBack();
          }}>
            <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: 'red' }}>Batal</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleSaveContact} style={{ backgroundColor: 'red', width: Dimensions.get('window').width * 0.70, height: 40, borderRadius: 15, marginLeft: 15 }}>
            <Text style={{ textAlign: 'center', marginTop: 10, color: 'white', fontFamily: 'Poppins-Regular', fontSize: 12 }}>Simpan</Text>
          </TouchableOpacity>
        </View>
      </View>

    </View>
  );
};

export default TambahContactPage;
