import React, { useEffect, useState } from "react";
import { Dimensions, FlatList, Image, Text, TouchableOpacity, View, Modal, Alert, StyleSheet, ScrollView } from "react-native";
import Icons from 'react-native-vector-icons/Feather';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from "../../store/store";
import { deleteContactById, fetchData } from "../../store/slices/contact";

const HomepageScreen = ({ navigation }: any) => {
    const dispatch: AppDispatch = useDispatch();
    const [deleteByID,setDeleteByID] = useState(0);
    // Menggunakan useSelector untuk memilih state dari slice reducer yang sesuai
    const { data, loading, error } = useSelector((state: RootState) => state.contact);
    const [modalVisible, setModalVisible] = useState(false);

    useEffect(() => {
        dispatch(fetchData());
    }, [dispatch]);

    const placeholderImage = 'https://via.placeholder.com/40'; // URL gambar placeholder (jika diperlukan)

    if (loading) {
        return (
            <View>
                <Text>Loading...</Text>
            </View>
        );
    }

    if (error) {
        return (
            <View style={{alignItems: "center",marginTop: Dimensions.get("screen").height * 0.30}}>
                <Text style={{color: "black"}}>Error: {error}</Text>
            </View>
        );
    }

    const handleDelete = (id: number) => {
        dispatch(deleteContactById(id));
      };

      const handleEdit = (id: number) => {
        navigation.navigate("EditContact", { id });
    };

    return (
        <View style={{flex: 1,backgroundColor: "white"}}>
            <ScrollView>
                <View style={{ paddingHorizontal: 15, marginTop: 15 }}>
                    <FlatList
                        data={data?.data}
                        renderItem={({ item, index }) => {
                            const imageUrl = item.photo ? { uri: item.photo } : { uri: placeholderImage };
                            return (
                                <>
                                    <View style={{ flexDirection: "row", alignItems: "center", marginVertical: 10 }}>
                                        <Image source={imageUrl} style={{ width: 40, height: 40, borderRadius: 35 }} />
                                        <View style={{ flexDirection: "column" }}>
                                            <Text style={{ color: "black", marginLeft: 10 }}>Nama Lengkap : {item.firstName} {item.lastName}</Text>
                                            <Text style={{ color: "black", marginLeft: 10 }}>Umur : {item.age} Tahun</Text>

                                            <View style={{ marginLeft: 10, flexDirection: "row", marginTop: 10 }}>
                                                <TouchableOpacity onPress={() => handleEdit(item.id)} style={{ backgroundColor: "red", width: Dimensions.get("screen").width * 0.30, height: 35, borderRadius: 5 }}>
                                                    <View style={{ flexDirection: "row", alignItems: "center", alignSelf: "center" }}>
                                                        <Icons name="edit" color={"white"} size={16} style={{ marginTop: 5 }} />
                                                        <Text style={{ color: "white", textAlign: "center", marginTop: 7, marginLeft: 6 }}>Edit Profile</Text>
                                                    </View>
                                                </TouchableOpacity>

                                                <TouchableOpacity onPress={() => {
                                                    setModalVisible(true)
                                                    setDeleteByID(item.id)
                                                }} style={{ marginLeft: 10, borderWidth: 1, width: Dimensions.get("screen").width * 0.25, borderColor: 'red', borderRadius: 5, height: 35 }}>
                                                    <View style={{ flexDirection: "row", alignItems: "center", alignSelf: "center" }}>
                                                        <Icons name="trash-2" color={"red"} size={16} style={{ marginTop: 5 }} />
                                                        <Text style={{ color: "red", textAlign: "center", marginTop: 7, marginLeft: 6 }}>Delete</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                    {index < data?.data.length - 1 ? <View style={{ borderBottomWidth: 1, borderBottomColor: "gray" }} /> : null}
                                </>
                            );
                        }}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>

                <View style={{ height: 90 }} />
            </ScrollView>

            <View style={{ position: "absolute", bottom: 25, alignSelf: 'flex-end', paddingRight: 25 }}>
                <TouchableOpacity onPress={() => {
                    navigation.navigate("TambahContact")
                }} style={{ backgroundColor: "red", width: Dimensions.get('window').width * 0.40, height: 40, borderRadius: 15 }}>
                    <Text style={{ color: "white", fontSize: 14, textAlign: "center", marginTop: 9 }}>Tambah Contact</Text>
                </TouchableOpacity>
            </View>

            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => setModalVisible(false)}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>Apakah anda yakin ingin menghapus ini?</Text>
                        <TouchableOpacity
                            style={[styles.button, styles.buttonClose]}
                            onPress={() => {
                                setModalVisible(false)
                                handleDelete(deleteByID)
                            }}
                        >
                            <Text style={styles.textStyle}>Ya</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    button: {
        borderRadius: 10,
        padding: 10,
        elevation: 2,
        marginTop: 10
    },
    buttonOpen: {
        backgroundColor: 'red',
    },
    buttonClose: {
        backgroundColor: 'red',
        width: 150
    },
    textStyle: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    modalText: {
        //   marginBottom: 15,
        textAlign: 'center',
        color: "black"
    },
});

export default HomepageScreen;
