import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';

interface Contact {
  id: number;
  firstName: string;
  lastName: string;
  age: string;
}

interface DataState {
  data: Contact[];
  contacts: { [id: number]: Contact };
  loading: boolean;
  error: string | null;
}

interface CreateContactPayload {
  firstName: string;
  lastName: string;
  age: string;
  photo?: string; // Optional photo field
}

const initialState: DataState = {
  data: [],
  contacts: {},
  loading: false,
  error: null,
};

// Async thunk action to fetch data
export const fetchData = createAsyncThunk<Contact[], void>(
  'data/fetchData',
  async () => {
    try {
      const response = await axios.get<Contact[]>('https://contact.herokuapp.com/contact');
      return response.data;
    } catch (error) {
      console.error('Error fetching data:', error);
      throw error;
    }
  }
);

// Async thunk action to delete a contact by ID
export const deleteContactById = createAsyncThunk<number, number>(
    'contact/deleteContactById',
    async (id: number) => {
      try {
        await axios.delete(`https://contact.herokuapp.com/contact/${id}`);
        return id; // Return the ID of the deleted contact
      } catch (error) {
        console.error(`Error deleting contact with ID ${id}:`, error);
        throw error; // Re-throw the error to be handled by Redux
      }
    }
  );

// Async thunk action to create a new contact
export const createContact = createAsyncThunk<Contact, CreateContactPayload>(
  'data/createContact',
  async (newContact: CreateContactPayload) => {
    try {
      const response = await axios.post<Contact>('https://contact.herokuapp.com/contact', newContact);
      return response.data;
    } catch (error) {
      console.error('Error creating contact:', error);
      throw error;
    }
  }
);

// Async thunk action to fetch a single contact by ID
export const getContact = createAsyncThunk<Contact, number>(
  'data/getContact',
  async (id: number) => {
    try {
      const response = await axios.get<Contact>(`https://contact.herokuapp.com/contact/${id}`);
      console.log(response.data?.data,'respon data get param id')
      return response.data;
    } catch (error) {
      console.error(`Error fetching contact with ID ${id}:`, error);
      throw error;
    }
  }
);

// Async thunk action to update an existing contact
export const updateContact = createAsyncThunk<Contact, { id: number; updatedContact: CreateContactPayload }>(
  'data/updateContact',
  async ({ id, updatedContact }: { id: number; updatedContact: CreateContactPayload }) => {
    try {
      const response = await axios.put<Contact>(`https://contact.herokuapp.com/contact/${id}`, updatedContact);
      return response.data;
    } catch (error) {
      console.error(`Error updating contact with ID ${id}:`, error);
      throw error;
    }
  }
);

const dataSlice = createSlice({
  name: 'data',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      // Handle pending state while fetching data
      .addCase(fetchData.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      // Handle fulfilled state after successful data fetch
      .addCase(fetchData.fulfilled, (state, action: PayloadAction<Contact[]>) => {
        state.loading = false;
        state.data = action.payload;
        state.error = null; // Clear error if data fetching is successful
      })
      // Handle rejected state if data fetch fails
      .addCase(fetchData.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      // Handle pending state while deleting
      .addCase(deleteContactById.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      // Handle fulfilled state after successful deletion
      .addCase(deleteContactById.fulfilled, (state, action: PayloadAction<number>) => {
        state.loading = false;
        // Filter out the deleted contact from state.data
        state.data = state.data.filter(contact => contact.id !== action.payload);
        state.error = null; // Clear error if deletion is successful
      })
      // Handle rejected state if deletion fails
      .addCase(deleteContactById.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      // Handle pending state while creating contact
      .addCase(createContact.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      // Handle fulfilled state after successful creation
      .addCase(createContact.fulfilled, (state, action: PayloadAction<Contact>) => {
        state.loading = false;
        state.data.push(action.payload); // Add newly created contact to state
        state.error = null; // Clear error if creation is successful
      })
      // Handle rejected state if creation fails
      .addCase(createContact.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      // Handle pending state while fetching a single contact
      .addCase(getContact.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      // Handle fulfilled state after successful fetch of a single contact
      .addCase(getContact.fulfilled, (state, action: PayloadAction<Contact>) => {
        state.loading = false;
        // Replace or add the fetched contact to state.data
        const existingContactIndex = state.data.findIndex(contact => contact.id === action.payload.id);
        if (existingContactIndex !== -1) {
          state.data[existingContactIndex] = action.payload;
        } else {
          state.data.push(action.payload);
        }
        state.error = null; // Clear error if fetch is successful
      })
      // Handle rejected state if fetch of a single contact fails
      .addCase(getContact.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      // Handle pending state while updating a contact
      .addCase(updateContact.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      // Handle fulfilled state after successful update of a contact
      .addCase(updateContact.fulfilled, (state, action: PayloadAction<Contact>) => {
        state.loading = false;
        // Update the contact in state.data with the updated information
        const updatedContactIndex = state.data.findIndex(contact => contact.id === action.payload.id);
        if (updatedContactIndex !== -1) {
          state.data[updatedContactIndex] = action.payload;
        }
        state.error = null; // Clear error if update is successful
      })
      // Handle rejected state if update of a contact fails
      .addCase(updateContact.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      });
  },
});

export default dataSlice.reducer;
