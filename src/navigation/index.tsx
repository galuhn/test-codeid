import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomepageScreen from "../pages/Homepage";
import TambahContactPage from "../pages/TambahContact";
import EditContactPage from "../pages/EditContact";


// stack navigation
const Stack = createNativeStackNavigator();

const AppNavigation = () => {
    return <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen name="Home" component={HomepageScreen}  options={{
                title: 'Contact',
                headerStyle: {
                    backgroundColor: 'red',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                },
            }}/>
             <Stack.Screen name="TambahContact" component={TambahContactPage}  options={{
                title: 'Tambah Contact',
                headerStyle: {
                    backgroundColor: 'red',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                },
            }}/>
               <Stack.Screen name="EditContact" component={EditContactPage}  options={{
                title: 'Edit Contact',
                headerStyle: {
                    backgroundColor: 'red',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                },
            }}/>
        </Stack.Navigator>
    </NavigationContainer>
}

export default AppNavigation